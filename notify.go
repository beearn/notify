package notify

import (
	"fmt"
	"gitlab.com/beearn/entity"
	tele "gopkg.in/telebot.v3"
)

type Notify struct {
	tg                 *tele.Bot
	defaultRecipientID int64
}

type Opts func(*Notify)

func NewNotify(opts ...Opts) *Notify {
	n := &Notify{}

	for _, opt := range opts {
		opt(n)
	}

	if n.tg == nil {
		panic("telebot is required")
	}

	return n
}

func WithDefaultRecipientID(id int64) Opts {
	return func(n *Notify) {
		n.defaultRecipientID = id
	}
}

func WithTelegram(tg *tele.Bot) Opts {
	return func(n *Notify) {
		n.tg = tg
	}
}

func (n *Notify) Send(msg entity.MessageNotify) error {
	if n.tg == nil {
		return nil
	}
	if msg.RecipientID == 0 {
		msg.RecipientID = n.defaultRecipientID
	}
	if msg.RecipientID == 0 {
		return fmt.Errorf("recipient id is required")
	}

	var tgOpts []interface{}
	recipient := &tele.User{ID: msg.RecipientID}
	if msg.Format == "md" {
		tgOpts = append(tgOpts, tele.ModeMarkdown)
	}
	_, err := n.tg.Send(recipient, msg.Text, tgOpts...)

	return err
}
