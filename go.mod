module gitlab.com/beearn/notify

go 1.19

require (
	gitlab.com/beearn/entity v1.0.0
	gopkg.in/telebot.v3 v3.1.3
)
